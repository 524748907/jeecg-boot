package org.jeecg.modules.pwd.controller.v1;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.pwd.entity.PwdCategory;
import org.jeecg.modules.pwd.entity.PwdCategoryPassword;
import org.jeecg.modules.pwd.service.IPwdCategoryPasswordService;
import org.jeecg.modules.pwd.service.IPwdCategoryService;
import org.jeecg.modules.system.entity.SysPermission;
import org.jeecg.modules.system.entity.SysUser;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;

/**
 * 密码助手API
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/api/pwd/")
@Slf4j
public class ApiPwdController {
	@Autowired
	private IPwdCategoryService pwdCategoryService;
	@Autowired
	private IPwdCategoryPasswordService pwdCategoryPasswordService;
	
	/**
	 * 查询分类列表
	 * @param pwdCategory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@GetMapping(value = "/category_list")
	public Result<List> category_list(@RequestParam(name="uid", defaultValue="") String uid,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="20") Integer pageSize) {
		Result<List> result = new Result<List>();
		List list = pwdCategoryService.query_list(uid, pageNo, pageSize);
		
		result.setResult(list);
		result.success("OK");
		return result;
	}
	
	
	/**
	 * 查询密码列表
	 * @param pwdCategoryPassword
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@GetMapping(value = "/password_list")
	public Result<List> password_list(@RequestParam(name="cid", defaultValue="") String cid,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
		Result<List> result = new Result<List>();
		List list = pwdCategoryPasswordService.query_list(cid, pageNo, pageSize);
		result.setResult(list);
		result.success("OK");
		return result;
	}
	

}

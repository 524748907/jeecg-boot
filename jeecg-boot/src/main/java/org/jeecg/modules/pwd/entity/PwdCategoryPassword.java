package org.jeecg.modules.pwd.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 密码配置
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
@Data
@TableName("pwd_category_password")
public class PwdCategoryPassword implements Serializable {
    private static final long serialVersionUID = 1L;
    
	/**主键*/
	@TableId(type = IdType.UUID)
	private java.lang.String id;
	/**账号*/
	@Excel(name = "账号", width = 15)
	private java.lang.String uid;
	/**分类编号*/
	@Excel(name = "分类编号", width = 15)
	private java.lang.String categoryId;
	/**名称*/
	@Excel(name = "名称", width = 15)
	private java.lang.String title;
	/**账号*/
	@Excel(name = "账号", width = 15)
	private java.lang.String username;
	/**密码*/
	@Excel(name = "密码", width = 15)
	private java.lang.String password;
	/**备注*/
	@Excel(name = "备注", width = 15)
	private java.lang.Object msgContent;
	/**排序*/
	@Excel(name = "排序", width = 15)
	private Integer sort;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
	private java.lang.String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;
	/**修改人*/
	@Excel(name = "修改人", width = 15)
	private java.lang.String updateBy;
	/**修改时间*/
	@Excel(name = "修改时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private java.util.Date updateTime;
}

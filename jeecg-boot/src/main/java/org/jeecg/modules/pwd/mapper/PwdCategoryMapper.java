package org.jeecg.modules.pwd.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.pwd.entity.PwdCategory;
import org.jeecg.modules.system.entity.SysUserDepart;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 密码标签
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
public interface PwdCategoryMapper extends BaseMapper<PwdCategory> {
	
	public List query_list(@Param("uid") String uid,@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
}

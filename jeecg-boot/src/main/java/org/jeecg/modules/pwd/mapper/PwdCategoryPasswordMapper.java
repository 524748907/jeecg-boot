package org.jeecg.modules.pwd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.pwd.entity.PwdCategoryPassword;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 密码配置
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
public interface PwdCategoryPasswordMapper extends BaseMapper<PwdCategoryPassword> {
	
	public List query_list(@Param("cid") String cid,@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
}

package org.jeecg.modules.pwd.service;

import java.util.List;

import org.jeecg.modules.pwd.entity.PwdCategoryPassword;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 密码配置
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
public interface IPwdCategoryPasswordService extends IService<PwdCategoryPassword> {
	
	/**
	 * 查询密码配置详情
	 * @param cid
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List query_list(String cid,Integer pageNo,Integer pageSize);
}

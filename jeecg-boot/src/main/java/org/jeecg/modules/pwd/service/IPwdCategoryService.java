package org.jeecg.modules.pwd.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.pwd.entity.PwdCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 密码标签
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
public interface IPwdCategoryService extends IService<PwdCategory> {
	
	/**
	 * 查询密码分类列表
	 * @param uid
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List query_list(String uid, Integer pageNo, Integer pageSize);
}

package org.jeecg.modules.pwd.service.impl;

import java.util.List;

import org.jeecg.modules.pwd.entity.PwdCategoryPassword;
import org.jeecg.modules.pwd.mapper.PwdCategoryPasswordMapper;
import org.jeecg.modules.pwd.service.IPwdCategoryPasswordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 密码配置
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
@Service
public class PwdCategoryPasswordServiceImpl extends ServiceImpl<PwdCategoryPasswordMapper, PwdCategoryPassword> implements IPwdCategoryPasswordService {

	@Override
	public List query_list(String cid, Integer pageNo, Integer pageSize) {
		return this.baseMapper.query_list(cid, (pageNo - 1) * pageSize, pageSize);
	}

}

package org.jeecg.modules.pwd.service.impl;

import java.util.List;

import org.jeecg.modules.pwd.entity.PwdCategory;
import org.jeecg.modules.pwd.mapper.PwdCategoryMapper;
import org.jeecg.modules.pwd.service.IPwdCategoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 密码标签
 * @author： jeecg-boot
 * @date：   2019-05-15
 * @version： V1.0
 */
@Service
public class PwdCategoryServiceImpl extends ServiceImpl<PwdCategoryMapper, PwdCategory> implements IPwdCategoryService {

	@Override
	public List query_list(String uid, Integer pageNo, Integer pageSize) {
		return this.baseMapper.query_list(uid, (pageNo - 1) * pageSize, pageSize);
	}

}

package org.jeecg.modules.webcrawler.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.service.IWebCrawlerWordService;
import org.jeecg.modules.webcrawler.util.WebCrawlerCacheUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;

 /**
 * @Title: Controller
 * @Description: 关键字配置
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
@RestController
@RequestMapping("/webcrawler/webCrawlerWord")
@Slf4j
public class WebCrawlerWordController {
	@Autowired
	private IWebCrawlerWordService webCrawlerWordService;
	
	/**
	  * 分页列表查询
	 * @param webCrawlerWord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@GetMapping(value = "/list")
	public Result<IPage<WebCrawlerWord>> queryPageList(WebCrawlerWord webCrawlerWord,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									  HttpServletRequest req) {
		Result<IPage<WebCrawlerWord>> result = new Result<IPage<WebCrawlerWord>>();
		QueryWrapper<WebCrawlerWord> queryWrapper = new QueryWrapper<WebCrawlerWord>()
				.like(StringUtils.isNotEmpty(webCrawlerWord.getTitle()), "title", webCrawlerWord.getTitle())
				.orderByDesc("sort");
		
		Page<WebCrawlerWord> page = new Page<WebCrawlerWord>(pageNo, pageSize);
		IPage<WebCrawlerWord> pageList = webCrawlerWordService.page(page, queryWrapper);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}
	
	/**
	  *   添加
	 * @param webCrawlerWord
	 * @return
	 */
	@PostMapping(value = "/add")
	public Result<WebCrawlerWord> add(@RequestBody WebCrawlerWord webCrawlerWord) {
		Result<WebCrawlerWord> result = new Result<WebCrawlerWord>();
		try {
			webCrawlerWordService.save(webCrawlerWord);
			String key = "web_crawler_word_list";
			WebCrawlerCacheUtils.del(key);
			result.success("添加成功！");
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
			result.error500("操作失败");
		}
		return result;
	}
	
	/**
	  *  编辑
	 * @param webCrawlerWord
	 * @return
	 */
	@PutMapping(value = "/edit")
	public Result<WebCrawlerWord> edit(@RequestBody WebCrawlerWord webCrawlerWord) {
		Result<WebCrawlerWord> result = new Result<WebCrawlerWord>();
		WebCrawlerWord webCrawlerWordEntity = webCrawlerWordService.getById(webCrawlerWord.getId());
		if(webCrawlerWordEntity==null) {
			result.error500("未找到对应实体");
		}else {
			boolean ok = webCrawlerWordService.updateById(webCrawlerWord);
			//TODO 返回false说明什么？
			if(ok) {
				String key = "web_crawler_word_list";
				WebCrawlerCacheUtils.del(key);
				result.success("修改成功!");
			}
		}
		
		return result;
	}
	
	/**
	  *   通过id删除
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/delete")
	public Result<WebCrawlerWord> delete(@RequestParam(name="id",required=true) String id) {
		Result<WebCrawlerWord> result = new Result<WebCrawlerWord>();
		WebCrawlerWord webCrawlerWord = webCrawlerWordService.getById(id);
		if(webCrawlerWord==null) {
			result.error500("未找到对应实体");
		}else {
			boolean ok = webCrawlerWordService.removeById(id);
			if(ok) {
				String key = "web_crawler_word_list";
				WebCrawlerCacheUtils.del(key);
				result.success("删除成功!");
			}
		}
		
		return result;
	}
	
	/**
	  *  批量删除
	 * @param ids
	 * @return
	 */
	@DeleteMapping(value = "/deleteBatch")
	public Result<WebCrawlerWord> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		Result<WebCrawlerWord> result = new Result<WebCrawlerWord>();
		if(ids==null || "".equals(ids.trim())) {
			result.error500("参数不识别！");
		}else {
			String key = "web_crawler_word_list";
			WebCrawlerCacheUtils.del(key);
			this.webCrawlerWordService.removeByIds(Arrays.asList(ids.split(",")));
			result.success("删除成功!");
		}
		return result;
	}
	
	/**
	  * 通过id查询
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/queryById")
	public Result<WebCrawlerWord> queryById(@RequestParam(name="id",required=true) String id) {
		Result<WebCrawlerWord> result = new Result<WebCrawlerWord>();
		WebCrawlerWord webCrawlerWord = webCrawlerWordService.getById(id);
		if(webCrawlerWord==null) {
			result.error500("未找到对应实体");
		}else {
			result.setResult(webCrawlerWord);
			result.setSuccess(true);
		}
		return result;
	}

  /**
      * 导出excel
   *
   * @param request
   * @param response
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, HttpServletResponse response) {
      // Step.1 组装查询条件
      QueryWrapper<WebCrawlerWord> queryWrapper = null;
      try {
          String paramsStr = request.getParameter("paramsStr");
          if (oConvertUtils.isNotEmpty(paramsStr)) {
              String deString = URLDecoder.decode(paramsStr, "UTF-8");
              WebCrawlerWord webCrawlerWord = JSON.parseObject(deString, WebCrawlerWord.class);
              queryWrapper = QueryGenerator.initQueryWrapper(webCrawlerWord, request.getParameterMap());
          }
      } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
      }

      //Step.2 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      List<WebCrawlerWord> pageList = webCrawlerWordService.list(queryWrapper);
      //导出文件名称
      mv.addObject(NormalExcelConstants.FILE_NAME, "关键字配置列表");
      mv.addObject(NormalExcelConstants.CLASS, WebCrawlerWord.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("关键字配置列表数据", "导出人:Jeecg", "导出信息"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
  }

  /**
      * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<WebCrawlerWord> listWebCrawlerWords = ExcelImportUtil.importExcel(file.getInputStream(), WebCrawlerWord.class, params);
              for (WebCrawlerWord webCrawlerWordExcel : listWebCrawlerWords) {
                  webCrawlerWordService.save(webCrawlerWordExcel);
              }
              return Result.ok("文件导入成功！数据行数：" + listWebCrawlerWords.size());
          } catch (Exception e) {
              log.error(e.getMessage());
              return Result.error("文件导入失败！");
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
  }

}

package org.jeecg.modules.webcrawler.controller.v1;

import java.util.List;
import java.util.Map;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.webcrawler.service.IWebCrawlerArticleService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 警情头条  API
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/api/webcrawler/")
@Slf4j
public class ApiWebCrawlerController {
	
	@Autowired
    private RedisUtil redisUtil;
	
	@Autowired
	private IWebCrawlerArticleService webCrawlerArticleService;
	
	/**
	 * http://localhost:8080/jeecg-boot/api/webcrawler/query_list
	 * @param uid
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@GetMapping(value = "/query_list")
	public Result<List> category_list(@RequestParam(name="type", defaultValue="1") Integer type,
											@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											@RequestParam(name="pageSize", defaultValue="20") Integer pageSize) {
		Result<List> result = new Result<List>();
		List list = webCrawlerArticleService.queryList(type, pageNo, pageSize);
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	//http://localhost/api/webcrawler/top_ranking?type=baidu
	@GetMapping(value = "/top_ranking")
	public Result<Object> category_list(@RequestParam(name="type", defaultValue="weixin") String type) {
		Result<List> result = new Result<List>();
		if(type.equals("weibo")) {
			return Result.ok(redisUtil.get("top_ranking_weibo"));
		}else if(type.equals("weixin")) {
			return Result.ok(redisUtil.get("top_ranking_weixin"));
		}else {
			get_top_baidu(111);
			return Result.ok(redisUtil.get("top_ranking_baidu"));
		}
	}
	
	

	@GetMapping(value = "/get_pie_chart")
	public Result<List> get_pie_chart(@RequestParam(name="type", defaultValue="1") Integer type) {
		Result<List> result = new Result<List>();
		List list = webCrawlerArticleService.getPieChart(type);
		result.success("OK");
		result.setResult(list);
		return result;
	}

	
	public  List<Map> get_top_baidu(int type){
		//整个html内容
		Document doc;
		List<Map> list = Lists.newArrayList();
		try {
			//Thread.sleep(10000);	//
			Connection conn = Jsoup.connect("http://top.baidu.com/buzz?b=1&fr=topindex").timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			doc = conn.get();
			log.info("***************************************初始化  搜索热词-百度********************************************");
			Elements tablelist = doc.select(".list-table tr");
			tablelist.remove(0);
			tablelist.remove(1);
			tablelist.remove(2);
			tablelist.remove(3);
			if(!tablelist.isEmpty()) {
				for (Element info : tablelist) {
					Map<String, String> map = Maps.newConcurrentMap();
					map.put("title", info.select("a.list-title").text());
					map.put("url", info.select("a.list-title").attr("href"));
					System.out.println(info.select("td.last").text());
					map.put("count", info.select("td.last").text());
					System.out.println("-----------------------------------");
					list.add(map);
				}
			}
			if(list != null && list.size() > 0){
				String key = "top_ranking_baidu";
				redisUtil.set(key, list);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return list;
		} 
	}

	

}

package org.jeecg.modules.webcrawler.job;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.util.WebCrawlerCacheUtils;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomFilterHelper;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomRedisService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

/**
 * 河北长城网
 * @author Scott
 */
@Slf4j
public class HebeiJob implements Job {
	
	@Autowired
    private BloomRedisService redisService;

    @Autowired
    private BloomFilterHelper bloomFilterHelper;
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		log.info(String.format(" 河北长城网 !  时间:" + DateUtils.getTimestamp()));
		List<WebCrawlerWord> wordList = WebCrawlerCacheUtils.queryWordList();
		for (WebCrawlerWord word : wordList) {
			System.out.println(word.getTitle());
			if(word.getSort() == 1) {		//公安
				get_list(word.getTitle(), 0, 6);
			}else if(word.getSort() == 2) {//经开区
				get_list(word.getTitle(), 0, 106);
			}else if(word.getSort() == 3) {//经开区
				get_list(word.getTitle(), 0, 306);
			}else if(word.getSort() == 667) {//王东峰
				get_list(word.getTitle(), 0, 667);
			}else if(word.getSort() == 668) {//高宏志
				get_list(word.getTitle(), 0, 668);
			}
			
			System.out.println("-------------------------------------");
		}
	}
	
	
	/**
	 *河北长城网
	 */
	public  void get_list(String keyword,int page,int type){
		//整个html内容
		Document doc;
		int errcount = 0; //重复次数
		try {
			Thread.sleep(1000);	//
			//http://zhannei.baidu.com/cse/site?q=%E9%82%AF%E9%83%B8%E8%AD%A6%E5%AF%9F&p=2&cc=hebei.com.cn
			Connection conn = Jsoup.connect("http://zhannei.baidu.com/cse/site?q="+URLEncoder.encode(keyword, "UTF-8")+"&cc=hebei.com.cn&submit.x=0&submit.y=0&p="+page).timeout(5000);
			//Connection conn = Jsoup.connect("http://zhannei.baidu.com/cse/site?q=" +URLEncoder.encode(keyword, "UTF-8") + "&p="+page+"&cc=hebei.com.cn").timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36");
			doc = conn.get();
			String name = doc.getElementsByTag("title").text();
			log.info("***************************************"+keyword+"*****第"+page+"页("+name+")********************************************");
			Elements tablelist = doc.getElementById("results").children();
			tablelist.remove(0);
			if(!tablelist.isEmpty()) {
				
				for (Element info : tablelist) {
					//System.out.println(info.html());
					
					String title = info.select("a").first().text();
					String url = info.select("a").first().attr("href");
					String time = info.select(".c-showurl").text();
					time = time.substring(time.lastIndexOf(" ")+1).trim();
					
					if(StringUtils.isNotEmpty(url)) {		//爬虫过滤重复url
						if(redisService.includeByBloomFilter(bloomFilterHelper, "www.hebei.com.cn"+type, url)){  //url已存在
							errcount++;
						}else {
							redisService.addByBloomFilter(bloomFilterHelper, "www.hebei.com.cn"+type, url);
							if(WebCrawlerCacheUtils.getTotalCount(url, type) > 0) {
								errcount++;
								break;
							}else {
								Date date = DateUtils.stringToDate(time, "yyyy-MM-dd");
								WebCrawlerCacheUtils.addArticle(title, url, date, type, "长城网");
							}
						}
					}
					
					log.info(title);
					log.info(url);
					log.info(time);
					
					log.info("----------------------------------------重复次数"+errcount+"--------------------------------------------------");
				}
			}

			//查询分页列表
			page++;
			if(!tablelist.isEmpty() && errcount < 8) {
				//get_list(keyword, page,type);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 设置连接超时时间 
	}
	
	
	
	public static void main(String[] args) {
		String[] keywords = {"邯郸民警","破获","扫黑除恶","治安拘留","黄赌毒","邯郸警方","邯郸公安"};
//		for (String keyword : keywords) {
//			log.info(keyword);
//			new HebeiJob().get_list(keyword, 0);
//		}
		new HebeiJob().get_list("扫黑除恶", 38, 6);

	}
}

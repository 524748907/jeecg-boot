package org.jeecg.modules.webcrawler.job;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.util.WebCrawlerCacheUtils;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomFilterHelper;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomRedisService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

/**
 * 河北新闻网
 * 
 * @author Scott
 */
@Slf4j
public class HebnewsJob implements Job {
	@Autowired
    private BloomRedisService redisService;

    @Autowired
    private BloomFilterHelper bloomFilterHelper;
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		log.info(String.format(" 河北新闻网!  时间:" + DateUtils.getTimestamp()));
		List<WebCrawlerWord> wordList = WebCrawlerCacheUtils.queryWordList();
		for (WebCrawlerWord word : wordList) {
			if(word.getSort() == 1) {		//公安
				get_list(word.getTitle(), 1, 3);
			}else if(word.getSort() == 2) {//经开区
				get_list(word.getTitle(), 1, 103);
			}else if(word.getSort() == 3) {//经开区
				get_list(word.getTitle(), 1, 303);
			}else if(word.getSort() == 667) {//王东峰
				get_list(word.getTitle(), 1, 667);
			}else if(word.getSort() == 668) {//高宏志
				get_list(word.getTitle(), 1, 668);
			}
		}
	}
	
	
	/**
	 * 邯郸新闻网
	 */
	public  void get_list(String keyword,int page,int type){
		//整个html内容
		Document doc;
		int errcount = 0;
		try {
			Connection conn = Jsoup.connect("http://search.hebnews.cn:8070/servlet/SearchServlet.do?contentKey="+keyword+"&titleKey=&authorKey=&nodeNameResult=&subNodeResult=&dateFrom=&dateEnd=&sort=date&op=single&siteID=&pager.offset="+(page-1)*10+"&pageNo="+page).timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			doc = conn.get();
			
			String name = doc.getElementsByTag("title").text();
			log.info("***************************************"+keyword+"*****第"+page+"页("+name+")********************************************");
			
			Elements tablelist = doc.select("div#result_list table");
			if(!tablelist.isEmpty()) {
				Elements list = tablelist.get(1).getElementsByTag("td");
				for (Element info : list) {
					System.out.println(info.select("a").html());
					String title = info.select("a").first().text();
					String url = info.select("a").first().attr("href");
					String time = "";
					if(StringUtils.isNotEmpty(url)) {		//爬虫过滤重复url
						int index = url.indexOf("/content_");
						time = url.substring(index-10, index).replace("/", "-");
						if(time.equals("s.cn-house")) {
							continue;
						}
						
						if(redisService.includeByBloomFilter(bloomFilterHelper, "search.hebnews.cn"+type, url)){  //url已存在
							errcount++;
						}else {
							redisService.addByBloomFilter(bloomFilterHelper, "search.hebnews.cn"+type, url);
							if(WebCrawlerCacheUtils.getTotalCount(url, type) > 0) {
								errcount++;
								break;
							}else {
								Date date = DateUtils.stringToDate(time, "yyyy-MM-dd");
								WebCrawlerCacheUtils.addArticle(title, url, date, type, "河北新闻网");
							}
						}
				}
					
					log.info(title);
					log.info(url);
					log.info(time);
					
					log.info("----------------------------------------重复次数"+errcount+"--------------------------------------------------");

				}
			}
			
		
			//查询分页列表
			page++;
			if(!tablelist.isEmpty() && errcount < 8) {
				//get_list(keyword, page, type);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 设置连接超时时间 
	}
	
	public  String getTime(String url){
		//整个html内容
		Document doc;
		boolean flag = true;
		try {
			Connection conn = Jsoup.connect(url).timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			doc = conn.get();
			//打印html文档的<title>内容
			String time = null;
			if(doc.select(".am-article-meta").hasText()) {
				time = doc.select(".am-article-meta").text();
				time = time.substring(time.length() - 19);
			}else if(doc.select(".post_source").hasText()) {
				time = doc.select(".post_source").first().text();
				time = time.substring(time.length() - 19);
			}else if(doc.select("time").hasText()) {
				int index = url.indexOf("-");
				time = url.substring(index - 4, index+1);
				time += doc.select("time").first().text();
			}else if(doc.select(".i-time").hasText()) {
				time = doc.select(".i-time").first().text();
				time = time.substring(time.length() - 19);
			}else if(doc.select(".pub_time").hasText()) {
				time = doc.select(".pub_time").first().text();
				time = time.substring(time.length() - 19);
			}else if(doc.select(".source").hasText()) {
				time = doc.select(".source").first().text().replace("来源：人民网 　", "");
				time = time.substring(0,19);
			}
		
			
			System.out.println(time);
			return time;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info("详情页面不存在");
			return null;
		} // 设置连接超时时间 
	
	}
	
	
	public static void main(String[] args) {
//		String[] keywords = {"邯郸民警","破获","扫黑除恶","治安拘留","黄赌毒","邯郸警方","邯郸公安"};
//		for (String keyword : keywords) {
//			log.info(keyword);
//			new HebnewsJob().get_list(keyword, 1);
//		}
		new HebnewsJob().get_list("扫黑除恶", 15, 1);
	}
}

package org.jeecg.modules.webcrawler.job;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.util.WebCrawlerCacheUtils;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomFilterHelper;
import org.jeecg.modules.webcrawler.util.bloomfilter.BloomRedisService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

/**
 * 人民网
 * @author Scott
 */
@Slf4j
public class PeopleJob implements Job {
	
	@Autowired
    private BloomRedisService redisService;

    @Autowired
    private BloomFilterHelper bloomFilterHelper;
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		log.info(String.format(" 人民网 !  时间:" + DateUtils.getTimestamp()));
//		List<WebCrawlerWord> wordList = WebCrawlerCacheUtils.queryWordList();
//		for (WebCrawlerWord word : wordList) {
//			get_list(word.getTitle(), 1);
//		}
//		
		List<WebCrawlerWord> wordList = WebCrawlerCacheUtils.queryWordList();
		for (WebCrawlerWord word : wordList) {
			System.out.println(word.getTitle());
			if(word.getSort() == 1) {		//公安
				get_list(word.getTitle(), 1, 5);
			}else if(word.getSort() == 2) {//经开区
				get_list(word.getTitle(), 1, 105);
			}else if(word.getSort() == 3) {//经开区
				get_list(word.getTitle(), 1, 305);
			}else if(word.getSort() == 667) {//王东峰
				get_list(word.getTitle(), 1, 667);
			}else if(word.getSort() == 668) {//高宏志
				get_list(word.getTitle(), 1, 668);
			}
			
			System.out.println("-------------------------------------");
		}
	}
	
	
	/**
	 *人民网
	 */
	public  void get_list(String keyword,int page, int type){
		//整个html内容
		Document doc;
		int errcount = 0; //重复次数
		try {
			//Thread.sleep(1000);	//
			//http://www.handannews.com.cn:9088/servlet/SearchServlet.do?contentKey=%E9%82%AF%E9%83%B8&titleKey=&authorKey=&nodeNameResult=&subNodeResult=&dateFrom=&dateEnd=&sort=&op=single&siteID=&pager.offset=20&pageNo=3  "++"&pageNo="+page
			Connection conn = Jsoup.connect("http://search.people.com.cn/cnpeople/search.do?pageNum="+page+"&keyword="+URLEncoder.encode(keyword, "GBK")+"&siteName=news&facetFlag=true&nodeType=belongsId&nodeId=0").timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			doc = conn.get();
			String name = doc.getElementsByTag("title").text();
			log.info("***************************************"+keyword+"*****第"+page+"页("+name+")********************************************");
			Elements tablelist = doc.select("div.fr.w800 ul");
			if(!tablelist.isEmpty()) {
				
				for (Element info : tablelist) {
					
					String title = info.select("a").first().text();
					String url = info.select("a").first().attr("href");
					String time = info.child(2).text();
					time = time.substring(time.length() - 19);
					
					if(StringUtils.isNotEmpty(url)) {		//爬虫过滤重复url
						if(redisService.includeByBloomFilter(bloomFilterHelper, "search.people.com.cn"+type, url)){  //url已存在
							errcount++;
						}else {
							redisService.addByBloomFilter(bloomFilterHelper, "search.people.com.cn"+type, url);
							if(WebCrawlerCacheUtils.getTotalCount(url, type) > 0) {
								errcount++;
								break;
							}else {
								Date date = DateUtils.str2Date(time, DateUtils.datetimeFormat);
								WebCrawlerCacheUtils.addArticle(title, url, date, type, "人民网");
							}
						}
					}
					
					log.info(title);
					log.info(url);
					log.info(time);
					
					log.info("----------------------------------------重复次数"+errcount+"--------------------------------------------------");
				}
			}
			

			//查询分页列表
			page++;
			if(!tablelist.isEmpty() && errcount < 8) {
				//get_list(keyword, page,type);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 设置连接超时时间 
	}
	
	public  String getTime(String url){
		//整个html内容
		Document doc;
		boolean flag = true;
		try {
			Connection conn = Jsoup.connect(url).timeout(5000);
			conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.header("Accept-Encoding", "gzip, deflate, sdch");
			conn.header("Accept-Language", "zh-CN,zh;q=0.8");
			conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			doc = conn.get();
			//打印html文档的<title>内容
			String time = doc.select("div.date-source span").first().text();
			System.out.println(time);
			return time;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 设置连接超时时间 
		return null;
	}
	
	
	public static void main(String[] args) {
		String[] keywords = {"邯郸民警","破获","扫黑除恶","治安拘留","黄赌毒","邯郸警方","邯郸公安"};
		for (String keyword : keywords) {
			log.info(keyword);
			new PeopleJob().get_list(keyword, 1,1);
		}
		//new PeopleJob().get_list("邯郸公安", 3);
	}
}

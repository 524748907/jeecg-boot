package org.jeecg.modules.webcrawler.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.webcrawler.entity.WebCrawlerArticle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 文章列表
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
public interface WebCrawlerArticleMapper extends BaseMapper<WebCrawlerArticle> {
	
	/**
	 * 获取文章路径是否存在
	 * @param url
	 * @param type
	 * @return
	 */
	public Long getTotalCount(@Param("url") String url,@Param("type") Integer type);
	
	public List queryList(@Param("type") Integer type,@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
	public List getPieChart(@Param("type") Integer type);
}

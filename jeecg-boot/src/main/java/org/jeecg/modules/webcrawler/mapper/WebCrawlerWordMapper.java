package org.jeecg.modules.webcrawler.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 关键字配置
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
public interface WebCrawlerWordMapper extends BaseMapper<WebCrawlerWord> {

}

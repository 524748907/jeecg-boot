package org.jeecg.modules.webcrawler.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.webcrawler.entity.WebCrawlerArticle;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 文章列表
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
public interface IWebCrawlerArticleService extends IService<WebCrawlerArticle> {
	
	void addArticle(String title, String url, Date  time, Integer type,String createBy);
	
	Long getTotalCount( String url, Integer type);
	
	public List queryList(Integer type,Integer pageNo, Integer pageSize);
	
	public List getPieChart(Integer type);
}

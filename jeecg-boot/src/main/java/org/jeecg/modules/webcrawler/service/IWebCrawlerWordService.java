package org.jeecg.modules.webcrawler.service;

import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 关键字配置
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
public interface IWebCrawlerWordService extends IService<WebCrawlerWord> {

}

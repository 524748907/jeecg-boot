package org.jeecg.modules.webcrawler.service.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.util.IPUtils;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.system.entity.SysLog;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.webcrawler.entity.WebCrawlerArticle;
import org.jeecg.modules.webcrawler.mapper.WebCrawlerArticleMapper;
import org.jeecg.modules.webcrawler.service.IWebCrawlerArticleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 文章列表
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
@Service
public class WebCrawlerArticleServiceImpl extends ServiceImpl<WebCrawlerArticleMapper, WebCrawlerArticle> implements IWebCrawlerArticleService {

	@Override
	public void addArticle(String title, String url, Date time, Integer type,String createBy) {
		WebCrawlerArticle artcle = new WebCrawlerArticle();
		//注解上的描述,操作日志内容
		artcle.setTime(time);
		artcle.setTitle(title);
		artcle.setUrl(url);
		artcle.setType(type);
		artcle.setCreateTime(time);
		artcle.setCreateBy(createBy);
		this.save(artcle);
	}

	@Override
	public Long getTotalCount(String url, Integer type) {
		return this.baseMapper.getTotalCount(url, type);
	}

	@Override
	public List queryList(Integer type, Integer pageNo, Integer pageSize) {
		return baseMapper.queryList(type, (pageNo - 1) * pageSize, pageSize);
	}
	
	@Override
	public List getPieChart(Integer type) {
		return baseMapper.getPieChart(type);
	}
	

}

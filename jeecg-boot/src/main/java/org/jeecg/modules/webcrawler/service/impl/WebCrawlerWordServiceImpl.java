package org.jeecg.modules.webcrawler.service.impl;

import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.mapper.WebCrawlerWordMapper;
import org.jeecg.modules.webcrawler.service.IWebCrawlerWordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 关键字配置
 * @author： jeecg-boot
 * @date：   2019-06-11
 * @version： V1.0
 */
@Service
public class WebCrawlerWordServiceImpl extends ServiceImpl<WebCrawlerWordMapper, WebCrawlerWord> implements IWebCrawlerWordService {

}

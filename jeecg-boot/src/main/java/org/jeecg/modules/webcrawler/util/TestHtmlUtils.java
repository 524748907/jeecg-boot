package org.jeecg.modules.webcrawler.util;

import org.springframework.web.util.HtmlUtils;

/**
 * 测试htmlUtils 功能
  *
 * 2012-12-27 下午11:22:53
 */
public class TestHtmlUtils {

    String html = "<P><p>Noranda Income Fund (TSX:NIF.UN) \n今日宣布Noranda OperatingTrust董事会已批准派发2015年7月份每优先单位达$0.04167的股息，该股息将在2015年8月25日向截至2015年7月31日在录的优先单位持有人派发。</p></P>";
    /**
     * 把html的标签特殊字符转换成普通字符
     */
     public void testhtmlEscape(){
        String value = HtmlUtils.htmlEscape(html);
        System.out.println(value);
    }
    /**
     * 把html的特殊字符转换成普通数字
     */
     public void testhtmlEscapeDecimal(){
        String value = HtmlUtils.htmlEscapeDecimal(html);
        System.out.println(value);
    }
    /**
     * 把html的特殊字符转换成符合Intel HEX文件的字符串
     */
     public void htmlEscapeHex(){
        String value = HtmlUtils.htmlEscapeHex(html);
        System.out.println(value);
    }
    /**
     * 把html的特殊字符反转换成html标签
     * 以上三种方法都可以反转换
     */
 
    public void htmlUnescape(String content){
        String tmp = HtmlUtils.htmlEscapeDecimal(content);
        System.out.println(tmp);

        String value = HtmlUtils.htmlUnescape(content);
		value=HtmlUtils.htmlUnescape(value);
        System.out.println(value.replaceAll("\n","</p><p>"));
        System.out.println(value.replaceAll("</?[^>]+>","").replaceAll("\n","</p><p>"));
    }
    public static void main(String[] args) {
    	TestHtmlUtils th = new TestHtmlUtils();
    	//th.htmlUnescape();
    	String html = "山东狠抓中央扫黑除恶督导&quot;回头看&quot;反馈问题整改";
    	th.htmlUnescape(html);
	}
}

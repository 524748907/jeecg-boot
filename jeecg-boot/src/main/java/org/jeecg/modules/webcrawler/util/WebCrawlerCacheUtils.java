/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.jeecg.modules.webcrawler.util;

import java.util.Date;
import java.util.List;

import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.service.IWebCrawlerArticleService;
import org.jeecg.modules.webcrawler.service.IWebCrawlerWordService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * 接口配置工具类
 * 
 * @author Administrator
 *
 */
@Service
public class WebCrawlerCacheUtils implements ApplicationContextAware {
	
	private static RedisUtil redisUtil;
	
	private static IWebCrawlerWordService webCrawlerWordService;
	private static IWebCrawlerArticleService webCrawlerArticleService;
	

    
    /**
     * 获取关键词列表
     * @param uid
     * @return
     */
    public static List<WebCrawlerWord> queryWordList() {
		String key = "web_crawler_word_list";
		Object result = redisUtil.get(key);
		if (result != null) {
			return (List<WebCrawlerWord>)result;
		}else{
			List<WebCrawlerWord> list = webCrawlerWordService.list();
			if(list != null && list.size() > 0){
				redisUtil.set(key, list);
			}
			return list;
		}
	}
    
    /**
     * 删除缓存
     * @param key
     */
    public static void del(String key) {
		redisUtil.del(key);
	}
    
    
    
    
   
    
   

    public static void addArticle(String title, String url, Date  time, Integer type,String createBy) {
    	try {
    		webCrawlerArticleService.addArticle(title, url, time, type,createBy);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    public static Long getTotalCount(String url, Integer type) {
    	return webCrawlerArticleService.getTotalCount(url, type);
    }


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		redisUtil = (RedisUtil) applicationContext.getBean("redisUtil");
		webCrawlerWordService = (IWebCrawlerWordService) applicationContext.getBean("webCrawlerWordServiceImpl");
		webCrawlerArticleService = (IWebCrawlerArticleService) applicationContext.getBean("webCrawlerArticleServiceImpl");

		
	}
}

package org.jeecg.modules.webcrawler.util.bloomfilter;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 〈RedisRunner-用于在项目启动时加载需要的redis相关内容〉
 *
 * @create 2019/1/22
 * @since 1.0.0
 */
@Slf4j
@Component
public class BloomRedisRunner implements CommandLineRunner {

    @Autowired
    private BloomRedisService redisService;

    @Autowired
    private BloomFilterHelper bloomFilterHelper;

    @Override
    public void run(String... args) throws Exception {
        log.info("**** RedisRunner ****");
//        List<WbUser> wbUsers = wbUserMapper.selectListForBloom();
//        // 初始化布隆过滤器内容
//        for (WbUser user : wbUsers) {
//            redisService.addByBloomFilter(bloomFilterHelper, "bloom", user.getName());
//        }
//        redisService.addByBloomFilter(bloomFilterHelper, "bloom", "http://lf.hebnews.cn/2019-06/12/content_7417188.htm");
//        System.out.println(redisService.includeByBloomFilter(bloomFilterHelper, "bloom", "http://lf.hebnews.cn/2019-06/12/content_7417188.htm"));;
//        System.out.println(redisService.includeByBloomFilter(bloomFilterHelper, "bloom", "http://m.hebnews.cn/ts/2019-06/12/content_7415759.htm"));;
//        System.out.println( redisService.includeByBloomFilter(bloomFilterHelper, "bloom", "22222"));
//        
//        redisService.addByBloomFilter(bloomFilterHelper, "bloom11", "http://m.hebnews.cn/ts/2019-06/12/content_7415759.htm");
//        System.out.println(redisService.includeByBloomFilter(bloomFilterHelper, "bloom11", "http://lf.hebnews.cn/2019-06/12/content_7417188.htm"));;
//        System.out.println(redisService.includeByBloomFilter(bloomFilterHelper, "bloom11", "http://m.hebnews.cn/ts/2019-06/12/content_7415759.htm"));;
//        System.out.println(redisService.includeByBloomFilter(bloomFilterHelper, "bloom11", "22222"));
    }
}


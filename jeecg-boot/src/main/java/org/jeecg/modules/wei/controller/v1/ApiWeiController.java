package org.jeecg.modules.wei.controller.v1;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.HttpRequest;
import org.jeecg.modules.wei.entity.WeiArticle;
import org.jeecg.modules.wei.service.IWeiArticleService;
import org.jeecg.modules.wei.service.IWeiCommentService;
import org.jeecg.modules.wei.service.IWeiCountDataService;
import org.jeecg.modules.wei.util.WeiCacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 微博、微信文章列表
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/api/wei/")
@Slf4j
public class ApiWeiController {
	
	@Autowired
	private IWeiArticleService weiArticleService;
	@Autowired
	private IWeiCommentService weiCommentService;
	
	@Autowired
	private IWeiCountDataService weiCountDataService;
	
	/**
	 * http://localhost:8080/jeecg-boot/api/wei/query_article_list
	 * @param uid
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@GetMapping(value = "/query_article_list")
	public Result<List> query_article_list(@RequestParam(name="type", defaultValue="1") Integer type,
											@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											@RequestParam(name="pageSize", defaultValue="20") Integer pageSize,
											@RequestParam(name="commentPageSize", defaultValue="30") Integer commentPageSize) {
		if(type == 3) {
			type = 3;
		}else if(type == 2) {
			type = 1808545011;
		}else {
			type = 12;
		}
		String word = HttpRequest.sendGet("http://gongan.hbweiyinqing.cn/f/v5/datav/getDatav", "key=comment_keyword");
		JSONArray wordarr = JSONArray.parseArray(word);
		Result<List> result = new Result<List>();
		List<Map> list = weiArticleService.queryList(type, pageNo, pageSize);
		for (Map info : list) {
			String msgid = info.get("id").toString();
			List<Map> comment = weiCommentService.queryList(msgid, 1, commentPageSize);
			int yujingcount = 0;
			if(comment.size() > 0) {
				for (Map item : comment) {
					String content = item.get("content").toString();
					for (int i = 0; i < wordarr.size(); i++) {
						String key = wordarr.getJSONObject(i).getString("content");
						if(content.contains(key)) {
							yujingcount++;
							break;
						}
					}
				}
				info.put("commentcount", comment.size());
				info.put("commentlist", comment);
			}else {
				info.put("commentcount", comment.size());
				info.put("commentlist", comment);
			}
			info.put("yujingcount", yujingcount);
		}
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	
	/**
	 * http://localhost:8080/jeecg-boot/api/wei/query_comment_list
	 * @param uid
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@GetMapping(value = "/query_comment_list")
	public Result<List> query_comment_list(@RequestParam(name="msgid", defaultValue="") String msgid,
											@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											@RequestParam(name="pageSize", defaultValue="20") Integer pageSize) {
		Result<List> result = new Result<List>();
		List list = weiCommentService.queryList(msgid, pageNo, pageSize);
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	
	@GetMapping(value = "/query_taotiao_article_list")
	public Result<JSONObject> query_taotiao_article_list(@RequestParam(name="uid", defaultValue="") String uid,
			@RequestParam(name="pageToken", defaultValue="0") String pageToken) {
		Result<JSONObject> result = new Result<JSONObject>();
		JSONObject article = WeiCacheUtils.get_article_list(uid, pageToken);
		JSONArray list = article.getJSONArray("data");
		for (int i = 0; i < list.size(); i++) {
			JSONObject info = list.getJSONObject(i);
			info.put("publishDateStr", info.getString("publishDateStr").replace("T", " "));
			int commentCount = info.getIntValue("commentCount");
			if(commentCount > 0) {
				JSONObject r = WeiCacheUtils.get_article_comment_list(info.getString("id"), "1");
				if(r != null) {
					info.put("commentlist", WeiCacheUtils.get_article_comment_list(info.getString("id"), "1"));
				}else {
					info.put("commentCount", 0);
				}
				
			}
		
		}
		result.success("OK");
		result.setResult(article);
		return result;
	}
	
	@GetMapping(value = "/query_taotiao_comment_list")
	public Result<JSONObject> query_taotiao_comment_list(@RequestParam(name="id", defaultValue="") String id,
			@RequestParam(name="pageToken", defaultValue="1") String pageToken) {
		Result<JSONObject> result = new Result<JSONObject>();
		JSONObject article = WeiCacheUtils.get_article_comment_list(id, pageToken);
		result.success("OK");
		result.setResult(article);
		return result;
	}
	///http://localhost:8080/jeecg-boot/api/wei/stat_member_list
	//微博、微信累计粉丝  7日数据
	@GetMapping(value = "/stat_member_list")
	public Result<List> stat_member_list(@RequestParam(name="type", defaultValue="1") Integer type) {
		Result<List> result = new Result<List>();
		String start = DateUtils.getYesterday("yyyy-MM-dd", -7);
		String end = DateUtils.getYesterday("yyyy-MM-dd", 0);
		List list = weiCountDataService.statCountList(type, start, end);
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	//微信7日阅读量排行 
	@GetMapping(value = "/stat_click_count_list")
	public Result<List> stat_click_count_list(@RequestParam(name="type", defaultValue="1") Integer type) {
		Result<List> result = new Result<List>();
		String start = DateUtils.getYesterday("yyyy-MM-dd", -8);
		String end = DateUtils.getYesterday("yyyy-MM-dd", 0);
		List list = weiArticleService.statClickCountList(type, start, end);
		result.success("OK");
		result.setResult(list);
		return result;
	}
	@GetMapping(value = "/stat_user_list")
	public Result<Map> stat_user_list(@RequestParam(name="type", defaultValue="1") Integer type) {
		Result<Map> result = new Result<Map>();
		String start = null;
		String end = null;
		if(type == 1) {
			start = DateUtils.getYesterday("yyyy-MM-dd", -4);
			end = DateUtils.getYesterday("yyyy-MM-dd", -1);
		}else {
			start = DateUtils.getYesterday("yyyy-MM-dd", -3);
			end = DateUtils.getYesterday("yyyy-MM-dd", 0);
		}
	
		List<Map> list = weiArticleService.statUserList(type, start, end);
		
		List<Map> plist = Lists.newArrayList();
		List<Map> zlist = Lists.newArrayList();
		int total_share_count = 0;
		int total_comment_count = 0;
		int total_click_count = 0;
		
		for (Map map : list) {
			int click_count = Integer.parseInt(map.get("click_count").toString());
			int comment_count = Integer.parseInt(map.get("comment_count").toString());
			int share_count = Integer.parseInt(map.get("share_count").toString()); 
			
			String time = map.get("time").toString();
			String create_by = map.get("create_by").toString();
			
			if(type == 1) {
				plist.add(getMapInfo(create_by, "阅读量", click_count, time));
				plist.add(getMapInfo(create_by, "评论数", comment_count, time));
				plist.add(getMapInfo(create_by, "分享数", share_count, time));
			}else {
				plist.add(getMapInfo(create_by, "点赞数", click_count, time));
				plist.add(getMapInfo(create_by, "评论数", comment_count, time));
				plist.add(getMapInfo(create_by, "转发数", share_count, time));
			}
			total_share_count = total_share_count + share_count;
			total_comment_count = total_comment_count + comment_count;
			total_click_count = total_click_count + click_count;
		}
		int total = total_share_count + total_comment_count + total_click_count;
		if(type == 1) {
			zlist.add(getMapInfo("分享数", total_share_count, total_share_count * 100 / total));
			zlist.add(getMapInfo("评论数", total_comment_count, total_comment_count * 100 / total));
			zlist.add(getMapInfo("阅读量", total_click_count, total_click_count * 100 / total));
		}else {
			if(total > 0) {
				zlist.add(getMapInfo("转发数", total_share_count, total_share_count * 100 / total));
				zlist.add(getMapInfo("评论数", total_comment_count, total_comment_count * 100 / total));
				zlist.add(getMapInfo("点赞数", total_click_count, total_click_count * 100 / total));
			}else {
				zlist.add(getMapInfo("转发数", total_share_count, 0));
				zlist.add(getMapInfo("评论数", total_comment_count, 0));
				zlist.add(getMapInfo("点赞数", total_click_count, 0));
			}
			
		}
		Map r = Maps.newConcurrentMap();
		r.put("zlist", zlist);
		r.put("plist", plist);
		r.put("list", list);
		
		result.success("OK");
		result.setResult(r);
		return result;
	}
	
	/**
	 * 各公众号排行  微信按阅读量
	 * @param type
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@GetMapping(value = "/query_top_article_list")
	public Result<List> query_top_article_list(@RequestParam(name="type", defaultValue="1") Integer type,
											@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											@RequestParam(name="pageSize", defaultValue="20") Integer pageSize) {
	
		Result<List> result = new Result<List>();
		String start = DateUtils.getYesterday("yyyy-MM-dd", -1*type);
		String end = DateUtils.getYesterday("yyyy-MM-dd", 0);
		List<Map> list = weiArticleService.queryTopList(type, start, end, pageSize);
		
		
		
	
		

		
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	private Map getMapInfo(String lable, String type, int total,String time) {
		Map map = Maps.newConcurrentMap();
		map.put("label", lable);
		map.put("type", type);
		map.put("value", total);
		map.put("time", time);
		return map;
	}
	
	private Map getMapInfo(String item, int count, int percent) {
		Map map = Maps.newConcurrentMap();
		map.put("item", item);
		map.put("count", count);
		map.put("percent", percent);
		return map;
	}

	

	@GetMapping(value = "/redis_test")
	public String redis_test(@RequestParam(name="uid", defaultValue="") String uid,
			@RequestParam(name="pageToken", defaultValue="0") String pageToken) {
		long time = WeiCacheUtils.getRedisCreateTime("time");
		long nowtime = new Date().getTime();
		if(nowtime > (time + 1000 * 60 * 60 * 4)) {
			Result<JSONObject> result = new Result<JSONObject>();
			JSONObject article = WeiCacheUtils.get_article_list(uid, pageToken);
			JSONArray list = article.getJSONArray("data");
			for (int i = 0; i < list.size(); i++) {
				JSONObject info = list.getJSONObject(i);
				int commentCount = info.getIntValue("commentCount");
				if(commentCount > 0) {
					info.put("commentlist", WeiCacheUtils.get_article_comment_list(info.getString("id"), "1"));
				}
			}
			result.success("OK");
			result.setResult(article);
		}
		
		return "";
	}
	
	

	
	/**
	  * 分页列表查询http://localhost:8080/jeecg-boot/api/wei/wei_article_list?type=1
	 * @param weiArticle
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@GetMapping(value = "/wei_article_list")
	public Result<IPage<WeiArticle>> queryPageList(WeiArticle weiArticle,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									  HttpServletRequest req) {
		Result<IPage<WeiArticle>> result = new Result<IPage<WeiArticle>>();
		QueryWrapper<WeiArticle> queryWrapper = QueryGenerator.initQueryWrapper(weiArticle, req.getParameterMap());
		queryWrapper.eq((weiArticle.getType() != null && weiArticle.getType() > 0), "type", weiArticle.getType());
		queryWrapper.orderByDesc("create_time");
		Page<WeiArticle> page = new Page<WeiArticle>(pageNo, pageSize);
		IPage<WeiArticle> pageList = weiArticleService.page(page, queryWrapper);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}
	//分页列表查询http://localhost:8080/jeecg-boot/api/wei/statPinlvList
	@GetMapping(value = "/statPinlvList")
	public Result<List> statPinlvList(@RequestParam(name="today", defaultValue="") String today,
									  HttpServletRequest req) {
		
		
		Result<List> result = new Result<List>();
		if(StringUtils.isEmpty(DateUtils.getYesterday("yyyy-MM-dd", -1))) {
			today = DateUtils.getYesterday("yyyy-MM-dd", -1);
		}
		List list = weiArticleService.statPinlvList(today);
		
		result.success("OK");
		result.setResult(list);
		return result;
	}
	
	public static void main(String[] args) {
		long nowtime = new Date().getTime();
		System.out.println(nowtime);
		System.out.println(nowtime + 1000 * 60 * 60 * 4);
	}
}

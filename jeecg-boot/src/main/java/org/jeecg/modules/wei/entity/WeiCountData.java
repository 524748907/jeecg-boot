package org.jeecg.modules.wei.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信、微博关注粉丝
 * @author： jeecg-boot
 * @date：   2019-07-05
 * @version： V1.0
 */
@Data
@TableName("wei_count_data")
public class WeiCountData implements Serializable {
    private static final long serialVersionUID = 1L;
    
	/**主键*/
	@Excel(name = "主键", width = 15)
	@TableId(type = IdType.UUID)
	private java.lang.String id;
	private java.lang.String uid;
	/**标题*/
	@Excel(name = "标题", width = 15)
	private java.lang.String time;
	/**类型*/
	@Excel(name = "类型", width = 15)
	private java.lang.Integer type;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
	private java.lang.String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;
	/**修改人*/
	@Excel(name = "修改人", width = 15)
	private java.lang.String updateBy;
	/**修改时间*/
	@Excel(name = "修改时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private java.util.Date updateTime;
	/**total*/
	@Excel(name = "total", width = 15)
	private java.lang.Integer total;
}

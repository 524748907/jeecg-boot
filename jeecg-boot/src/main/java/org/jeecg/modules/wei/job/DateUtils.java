/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package org.jeecg.modules.wei.job;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.time.DateFormatUtils;

import com.alibaba.druid.sql.visitor.functions.Now;

/**
 * 日期工具类, 继承org.apache.commons.lang.time.DateUtils类
 * @author ThinkGem
 * @version 2013-3-15
 */
public class DateUtils extends org.apache.commons.lang.time.DateUtils {
	
	public static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss","yyyy-MM-dd hh:mm:ss","yyyyMMdd" };

	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd）
	 */
	public static String getDate() {
		return getDate("yyyy-MM-dd");
	}
	
	
	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String getDate(String pattern) {
		return DateFormatUtils.format(new Date(), pattern);
	}
	
	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}

	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getTime() {
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getDateTime() {
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 得到当前日期和时间字符串 格式（yyyyMMddHHmmss）
	 */
	public static String getDateTimeString() {
		return formatDate(new Date(), "yyyyMMddHHmmss");
	}
	
	/**
	 * 得到当前日期和时间字符串 格式（yyyyMMddHHmmssSSS）
	 */
	public static String getDateTimeString2() {
		return formatDate(new Date(), "yyyyMMddHHmmssSSS");
	}
	
	/**
	 * 根据时间获得时分秒
	 * @param time
	 * @return
	 */
	public static long getNumberByDate(Date time) {
		return Long.parseLong(formatDate(time, "HHmmssSSS"));
	}
	
	
	public static String getNumberByDate1(Date time) {
		return formatDate(time,"MM月dd日 HH:mm:ss");
	}
	/**
	 * 根据时间获得时分秒
	 * @param time
	 * @return
	 */
	public static String getNumberByDateStr(Date time) {
		return formatDate(time, "yyyy-MM-dd HH:mm:ss:SSS");
	}
	
	public static String getNumberByDateStr1(Date time) {
		return formatDate(time, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getYear() {
		return formatDate(new Date(), "yyyy");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getMonth() {
		return formatDate(new Date(), "MM");
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getDay() {
		return formatDate(new Date(), "dd");
	}
	
	/**
	 * @author Jakemanse
	 * @time 2013-9-9  上午9:49:12
	 * @function <p> 得到当天字符串格式如  20130831 </p>
	 * @return
	 */
	public static String getTodayString(){
		return formatDate(new Date(), "yyyyMMdd");
	}

	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek() {
		return formatDate(new Date(), "E");
	}
	
	/**
	 * 日期型字符串转化为日期 格式（"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss.fff" ）
	 */
	public static Date parseDate(String str) {
		try {
			return parseDate(str, parsePatterns);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 获取过去的天数
	 * @param date
	 * @return
	 */
	public static long pastDays(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(24*60*60*1000);
	}
	
	/**
	 * 获取过去的天数
	 * @param date
	 * @return
	 */
	public static long pastHours(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(60*60*1000);
	}
	
    /**
	* 取本周7天的第一天（周一的日期）
	*/
	public static Date getNowWeekBegin() {
		Calendar c = new GregorianCalendar();
       c.setFirstDayOfWeek(Calendar.MONDAY);
       c.setTime(new Date());
       c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek()); // Monday
		Date monday = c.getTime();
		
		
		DateFormat df = DateFormat.getDateInstance();
		String preMonday = df.format(monday);
		
		return parseDate(preMonday + " 00:00:00");
	
	}
	
	/**
	* 取本周7天的最后一天天（周一的日期）
	*/
	public static Date getNowWeekEnd() {
		Calendar c = new GregorianCalendar();
       c.setFirstDayOfWeek(Calendar.MONDAY);
       c.setTime(new Date());
       c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6); // Sunday
		Date monday = c.getTime();
		
		DateFormat df = DateFormat.getDateInstance();
		String preMonday = df.format(monday);
		
		Date date = c.getTime();
	    date.setHours(23);
	    date.setMinutes(59);
	    date.setSeconds(59);
		
	    return date;
		
		
		//return parseDate(preMonday + " 23:59:59");
	
	}
	
	
	/**   
	 * 得到本月的第一天   
	 * @return   
	 */    
	public static Date getMonthFirstDay() {     
	    Calendar calendar = Calendar.getInstance();     
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));     
	    
	    
	    DateFormat df = DateFormat.getDateInstance();
		String preMonday = df.format(calendar.getTime());
		
		
		Date date = calendar.getTime();
	    date.setHours(0);
	    date.setMinutes(0);
	    date.setSeconds(0);
		return date;
		
		//return parseDate(preMonday + " 00:00:00");
	}     
	
	
	/***
	 * 得到本月的最后一天  
	 *@author xiaoqian
	 * @return
	 * 2014-6-16下午3:52:41
	 */
	public static Date getMonthLastDay() {     
	    Calendar calendar = Calendar.getInstance();     
	    calendar.set(Calendar.DAY_OF_MONTH, calendar     
	            .getActualMaximum(Calendar.DAY_OF_MONTH));     
	    
	    Date date = calendar.getTime();
	    date.setHours(23);
	    date.setMinutes(59);
	    date.setSeconds(59);
	    
//	    	DateFormat df = DateFormat.getDateInstance();
//	  		String lastDayTime = df.format(calendar.getTime());
	  	return date;
	}  
	
	
	
	public static String getYesterday(String format){
		Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE,-1);
        Date d=cal.getTime();

		 SimpleDateFormat sp=new SimpleDateFormat(format);
		return sp.format(d);
	}
	
	public static String getYesterday(String format,Integer day){
		Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE,day);
        Date d=cal.getTime();

		 SimpleDateFormat sp=new SimpleDateFormat(format);
		return sp.format(d);
	}
	
	/***
	 * 得到当前时间和本月最后一天的时间差  
	 *@author xiaoqian
	 * @return
	 * 2014-6-16下午3:52:41
	 */
	public static Integer getMonthLastDayCountByNow() {     
		long diff =getMonthLastDay().getTime()-new Date().getTime();
		long days = diff / (1000 * 60 * 60 * 24);
	  		return (int)days;
	}  
	
	/***
	 * 得到当前时间和本周最后一天的时间差  
	 *@author xiaoqian
	 * @return
	 * 2014-6-16下午3:52:41
	 */
	public static Integer getWeeksLastDayCountByNow() {     
		long diff =getMonthLastDay().getTime()-new Date().getTime();
		long days = diff / (1000 * 60 * 60 * 24);
	  		return (int)days;
	}  
	
	
	
	
	/**
	 * 获取当天时间开始
	 *@author xiaoqian
	 * @return
	 * 2015-3-1下午4:40:09
	 */
	public static Date getStartTime(){  
        Calendar todayStart = Calendar.getInstance();  
        todayStart.set(Calendar.HOUR_OF_DAY, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);  
        todayStart.set(Calendar.MILLISECOND, 0);  
        return todayStart.getTime();  
    }  
      
	
	/**
	 * 获取当天时间结束
	 *@author xiaoqian
	 * @return
	 * 2015-3-1下午4:40:25
	 */
	public static Date getEndTime(){  
        Calendar todayEnd = Calendar.getInstance();  
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);  
        todayEnd.set(Calendar.MINUTE, 59);  
        todayEnd.set(Calendar.SECOND, 59);  
        todayEnd.set(Calendar.MILLISECOND, 999);  
        return todayEnd.getTime();  
    }  
	
	/**
	 * 获取当天时间开始
	 *@author xiaoqian
	 * @return
	 * 2015-3-1下午4:40:09
	 */
	public static Date getStartTime(Date time){  
        Calendar todayStart = Calendar.getInstance(); 
        todayStart.setTime(time);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);  
        todayStart.set(Calendar.MILLISECOND, 0);  
        return todayStart.getTime();  
    }  
      
	
	/**
	 * 获取当天时间结束
	 *@author xiaoqian
	 * @return
	 * 2015-3-1下午4:40:25
	 */
	public static Date getEndTime(Date time){  
        Calendar todayEnd = Calendar.getInstance();  
        todayEnd.setTime(time);
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);  
        todayEnd.set(Calendar.MINUTE, 59);  
        todayEnd.set(Calendar.SECOND, 59);  
        todayEnd.set(Calendar.MILLISECOND, 999);  
        return todayEnd.getTime();  
    }  
	
	
	/**
	 * 计算两个日期之间的天数</br>
	 * 任何一个参数传空都会返回-1</br>
	 * 返回两个日期的时间差，不关心两个日期的先后</br>
	 * @param dateStart
	 * @param dateEnd
	 * @return
	 */
	public static long getDaysBetweenTwoDate(Date dateEnd){
		Date nowDate = new Date();
		if(null == dateEnd){
			return -1;
		}
		long l = dateEnd.getTime()-nowDate.getTime();
		l = l/(1000*60*60*24l);
		return l;
	}
	
	
	
	
	
	
	
	
	
	
	
	


	/**
	 * <p>
	 * Author:Jakemanse
	 * <p>
	 * <p>
	 * time: 2012-9-11 下午6:00:44
	 * <p>
	 * <p>
	 * function: 返回时间格式为：yyyymmdd 如：20120911
	 * <p>
	 * <p>
	 * @return
	 * <p>
	 */
	public static String getLastDayString() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}
	
	/**
	 * @author Jakemanse
	 * @time 2013-5-29  下午2:32:11
	 * @function <p> 返回N天前的日期   返回时间格式为：yyyymmdd 如：20120911  </p>
	 * @param day
	 * @return
	 */
	
	public static String getBeforeDaysString(int day){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -day);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}
	
	
	/**
	 * @author Jakemanse
	 * @time 2016-1-15  下午12:14:11
	 * @function <p> 指定日期的  前几天  日期  </p>
	 * @param date
	 * @param day
	 * @return
	 */
	public static String getBeforeDaysString(Date date,int day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -day);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date bdate = calendar.getTime();
		return format.format(bdate).toString();
	}
	
	
	
	/**
	 * @author Jakemanse
	 * @time 2013-9-28  下午1:55:30
	 * @function <p> 获取延迟N天后的 日期时间 </p>
	 * @param days
	 * @return
	 */
	public static Date getDelayDate(int days){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, days);
		Date date = calendar.getTime();
		date.setHours(23);
		date.setMinutes(59);
		date.setSeconds(59);
		
		return date;
	}
	

	/**
	 * 获取延迟N天后的 日期时间 
	 * @param currdate
	 * @param days
	 * @return
	 */
	public static Date getDelayDate(Date currdate,int days){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currdate);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}

	
	
	/**
	 * @author Jakemanse
	 * @time 2013-5-29  下午2:32:11
	 * @function <p> 返回N天后的日期   返回时间格式为：yyyymmdd 如：20120911  </p>
	 * @param day
	 * @return
	 */
	public static String getAfterDaysString(int day){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}
	
	
	/**
	 * @author Jakemanse
	 * @time 2016-1-15  下午12:15:17
	 * @function <p> 指定日期 的后几天 日期   返回   yyyymmdd  </p>
	 * @param date
	 * @param day
	 * @return
	 */
	public static String getAfterDaysString(Date date ,int day){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date adate = calendar.getTime();
		return format.format(adate).toString();
	}
	
	
	
	
	public static String getTomorrowString(){
		return getAfterDaysString(1);
	}
	

	
	
	public static String getTodayStringByFormat(String format){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		Date date = calendar.getTime();
		return fmt.format(date).toString();
	}



	public static String getMonthString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("MM");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}

	public static String getYearString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}
	public static String getYearMonthString(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}

	public static String getDayString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd");
		Date date = calendar.getTime();
		return format.format(date).toString();
	}

	public static String getNormalDateString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = calendar.getTime();
		return format.format(date).toString();

	}
	
	public static String getNormalDateString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date).toString();
	}


	/**
	 * * 把字符串转化为日期类型 ("2000/12-3号 10时02:3") * * @param date * @return * @throws
	 * ParseException
	 * @throws ParseException 
	 */
	public static Date parseStrToDate(String date) throws ParseException  {
		Date result = null;
		String parse = date.trim();
		parse = parse.replaceFirst("^[0-9]{4}([^0-9]?)", "yyyy$1");
		parse = parse.replaceFirst("^[0-9]{2}([^0-9]?)", "yy$1");
		parse = parse.replaceFirst("([^0-9]?)[0-9]{1,2}([^0-9]?)", "$1MM$2");
		parse = parse.replaceFirst("([^0-9]?)[0-9]{1,2}( ?)", "$1dd$2");
		parse = parse.replaceFirst("( )[0-9]{1,2}([^0-9]?)", "$1HH$2");
		parse = parse.replaceFirst("([^0-9]?)[0-9]{1,2}([^0-9]?)", "$1mm$2");
		parse = parse.replaceFirst("([^0-9]?)[0-9]{1,2}([^0-9]?)", "$1ss$2");
		DateFormat format = new SimpleDateFormat(parse);
		result = (Date) format.parse(date);
		return result;
	}
	
	
	
	public static int getDiffDaysTo(Date startDate,Date endDate){
		long startMill = startDate.getTime();
		long endMill = endDate.getTime();
		
		long diffn = (endMill-startMill)/(1000*60*60*24);
		
		return (int) diffn;
	}
	
	public static Integer getDiffHours(Date startDate,Date endDate){
		long startMill = startDate.getTime();
		long endMill = endDate.getTime();
		long houth = (endMill-startMill)/(60 * 60 * 1000);
		return (int) houth;
	}
	
	
	
	public static List<String> getCurrentMonthDays(){
		Calendar calendar =  Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) +1;
		int day = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		//int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		List<String> dayList = new ArrayList<String>();
		for(int i=1;i<=day;i++){
			StringBuffer strbffer = new StringBuffer();
			String strmonth = (month <10) ? "0"+month:month+"";
			String strday = (i <10) ? "0"+i:i+"";
			strbffer.append(year).append(strmonth).append(strday);
			dayList.add(strbffer.toString());
		}
		return dayList;
		
	}
	/**
	 * 
	 * @author zhuxingang
	 * @time 2013-6-1 下午04:55:17
	 * @function <p>获取指定月天数 yyyymmdd</p>
	 * @param month
	 * @return
	 */
	public static List<String> getMonthDays(int month){
		Calendar calendar =  Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int day = -1;
		if(month==2){
			
			if(year % 4 != 0 || year % 100 == 0 && year % 400 != 0){
				
				day = 28;
				
			}else{
				
				day = 29;
				
			}
			
		}else{
			
			if(month==4 || month==6 || month==9 || month==11){
				
				day=30;
				
			}else{
				
				day=31;
			}
		}
		List<String> dayList = new ArrayList<String>();
		for(int i=1;i<=day;i++){
			
			StringBuffer strbffer = new StringBuffer();
			String strmonth = (month <10) ? "0"+month:month+"";
			String strday = (i <10) ? "0"+i:i+"";
			strbffer.append(year).append(strmonth).append(strday);
			dayList.add(strbffer.toString());
			
		}
		return dayList;
	}
	/**
	 * 获取本年度月字符串 yyyymm
	 * @author zhuxingang
	 * @return
	 */
	public static List<String> getYearMonthList(){
		
		Calendar calendar =  Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		List<String> YearMonthList = new ArrayList<String>();
		YearMonthList.add("");
		for(int i=1;i<=12;i++){
			StringBuffer strbffer = new StringBuffer();
			if(i>9){
				strbffer.append(year).append(i);
			}else{
				strbffer.append(year).append("0").append(i);
			}
			YearMonthList.add(strbffer.toString());
		}
		
		return YearMonthList;
	}
	/**
	 * 获得当前月的上个月      格式：yyyymm
	 * @author zhuxingang
	 * @return
	 */
	public static String getPreMonth(){
		//获得当前月
		String currentMonth = getMonthString();
		Calendar calendar =  Calendar.getInstance();
		Integer year = calendar.get(Calendar.YEAR);
		String preMonth = "";
		if(currentMonth.equals("01")){
			
			preMonth = (year-1)+"12";
			
		}else if("0".equals(currentMonth.substring(0, 1))){
			
			preMonth = year.toString()+"0"+ (Integer.parseInt(currentMonth.substring(1))-1);
			
		}else if("10".equals(currentMonth)){
			
			preMonth = year.toString() +"0"+ (Integer.parseInt(currentMonth)-1);
			
		}else{
			
			preMonth = year.toString()+ (Integer.parseInt(currentMonth)-1);
			
		}
		return preMonth;
	}
	/**
	 * 
	 * @author zhuxingang
	 * @time 2013-7-31 上午11:42:34
	 * @function <p>获取当前日期的上周一，周日</p>
	 * @return
	 */
	public static Map<String,Object> getLastWeekDays(){
		
		Calendar calendar1 = Calendar.getInstance();//last Monday
		Calendar calendar2 = Calendar.getInstance();//last Sunday
		Map<String,Object> params = new HashMap<String,Object>();
		int dayOfWeek=calendar1.get(Calendar.DAY_OF_WEEK)-1;
		int offset1=1-dayOfWeek;
		int offset2=7-dayOfWeek;
		calendar1.add(Calendar.DATE, offset1-7);
		calendar2.add(Calendar.DATE, offset2-7);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		params.put("lastMonday", format.format(calendar1.getTime()).toString());
		params.put("lastSunday", format.format(calendar2.getTime()).toString());
		return params;
		
	}
	/**
	 * 
	 * @author zhuxingang
	 * @time 2013-7-31 下午01:06:30
	 * @function <p>获取当前日期的周一，周日</p>
	 * @return
	 */
	public static Map<String,Object> getCurrWeekDays(){
		
		Map<String,Object> params = new HashMap<String,Object>();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar currMonday = Calendar.getInstance();//current Monday
		Calendar currSunday = Calendar.getInstance();//current Sunday
		currMonday.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		currSunday.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
      //取本周第一天 最后一天  
		params.put("currMonday", format.format(currMonday.getTime()).toString());
		params.put("currSunday", format.format(currMonday.getTime().getTime()+(6 * 24*60*60*1000)).toString());
		return params;
	}
	/**
	 * 
	 * @author zhuxingang
	 * @time 2013-7-31 下午01:11:44
	 * @function <p>获取当前月第一天,最后一天</p>
	 * @return
	 */
	public static Map<String,Object> getCurrMonthBeginEnd(){
        
		Map<String,Object> params = new HashMap<String,Object>();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        //获取当前月第一天：
        Calendar c = Calendar.getInstance();    
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
        String currBeginMonth = format.format(c.getTime());
        //获取当前月最后一天
        Calendar ca = Calendar.getInstance();    
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));  
        String currEndMonth = format.format(ca.getTime());
        params.put("currBeginMonth", currBeginMonth);
        params.put("currEndMonth", currEndMonth);
        return params;
        
	}
	/**
	 * 
	 * @author zhuxingang
	 * @time 2013-7-31 下午01:11:44
	 * @function <p>获取当前月的上月第一天,最后一天</p>
	 * @return
	 */
	public static Map<String,Object> getpreMonthBeginEnd(){
		
		Map<String,Object> params = new HashMap<String,Object>();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		//获取前月的第一天
        Calendar   cal_1=Calendar.getInstance();//获取当前日期 
        cal_1.add(Calendar.MONTH, -1);
        cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
        String preBeginMonth = format.format(cal_1.getTime());
        //获取前月的最后一天
        Calendar cale = Calendar.getInstance();   
        cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天 
        String preEndMonth = format.format(cale.getTime());
        params.put("preBeginMonth", preBeginMonth);
        params.put("preMonthEnd", preEndMonth);
        return params;
	}
	
	public static String getDateString(Long longdate){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(longdate);
		return format.format(date).toString();
	}
	
	
	public static String formatDate(Date date,String strformat){
		SimpleDateFormat format = new SimpleDateFormat(strformat);
		return format.format(date).toString();
	}
	
	/**
	 * @author Jakemanse
	 * @time 2013-10-15  下午11:11:30
	 * @function <p> 获取当前天的起始时间 如  2013-10-12 00：00：00 </p>
	 * @return
	 */
	public static Date getDayStartTime(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		
		return calendar.getTime();
	}
	


	/**
	 * 获取指定时间的前多少分种的时间
	 * @param date 时间
	 * @param minute 分钟
	 * @return
	 */
	public static Date getDateFrontMinute(Date date, Integer minute){
		Date frontDate = new Date(date.getTime() - (minute*60000));
		return frontDate;
	}
	
	/**
	 * 获取指定时间的后多少分种的时间
	 * @param date 时间
	 * @param minute 分钟
	 * @return
	 */
	public static Date getDateBackMinute(Date date, Integer minute){
		Date backDate = new Date(date.getTime() + (minute*60000));
		return backDate;
	}
	
	
	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
		getDateFrontMinute(new Date(), 10);
		getDateBackMinute(new Date(), 10);
	}
	
	/**
	 * 10位int型的时间戳转换为String(yyyy-MM-dd HH:mm:ss)
	 * @param time
	 * @return
	 */
	public static String timestampToString(Integer time){
		//int转long时，先进行转型再进行计算，否则会是计算结束后在转型
		long temp = (long)time*1000;
		Timestamp ts = new Timestamp(temp);  
        String tsStr = "";  
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        try {  
            //方法一  
            tsStr = dateFormat.format(ts);  
            System.out.println(tsStr);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return tsStr;  
	}
	/**
	 * 10位时间戳转Date
	 * @param time
	 * @return
	 */
	public static Date TimestampToDate(Integer time){
		long temp = (long)time*1000;
		Timestamp ts = new Timestamp(temp);  
        Date date = new Date();  
        try {  
            date = ts;  
            //System.out.println(date);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return date;
	}
	/**
	 * Date类型转换为10位时间戳
	 * @param time
	 * @return
	 */
	public static Integer DateToTimestamp(Date time){
		Timestamp ts = new Timestamp(time.getTime());
		
		return (int) ((ts.getTime())/1000);
	}
}

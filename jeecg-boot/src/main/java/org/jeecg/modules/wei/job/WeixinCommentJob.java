package org.jeecg.modules.wei.job;

import java.util.List;
import java.util.Map;

import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.HttpRequest;
import org.jeecg.modules.wei.entity.WeiComment;
import org.jeecg.modules.wei.service.IWeiArticleService;
import org.jeecg.modules.wei.service.IWeiCommentService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

/**
 * 微信   文章列表、评论列表
 * @author Scott
 */
@Slf4j
public class WeixinCommentJob implements Job {
	
	@Autowired
	private IWeiArticleService weiArticleService;
	
	@Autowired
	private IWeiCommentService weiCommentService;
	/**
	 * 若参数变量名修改 QuartzJobController中也需对应修改
	 */
	private String parameter;

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		log.info(String.format(" 微信评论 定时器 !  时间:" + DateUtils.getTimestamp()));
		
		int days = Integer.parseInt(parameter);
		
		String start = DateUtils.getYesterday("yyyy-MM-dd", -1*days);
		String end = DateUtils.getYesterday("yyyy-MM-dd", -1);
		List<Map<String,String>> msgidlist = weiArticleService.queryMsgidList(1, start, end);
		for (Map<String,String> map : msgidlist) {
			String accessToken = get_token(map.get("uid")); 
			getcommentlist(accessToken, map.get("msgid"));
		}
		
	}
	
	
	
	public String get_token(String uid) {
//		String token = HttpRequest.sendGet("http://yinqing.hbweiyinqing.cn/api/dama/get_access_token", "uid="+uid);
//		System.out.println("获取token"+token);
		String token = HttpRequest.sendGet("http://gongan.hbweiyinqing.cn/f/mp/api/getAccessToken", "appid=wx15f4f748100f9b2f");
		System.out.println("获取token"+token);
		return token;
	}
	
	public  void getcommentlist(String accessToken,String msgid) {
		JSONObject param = new JSONObject();
		String[] arr = msgid.split("_");
		param.put("msg_data_id", arr[0]);
		param.put("index", Integer.parseInt(arr[1]) - 1);
		param.put("begin", "0");
		param.put("count", "50");
		param.put("type", "2");
		
		
		String result = HttpRequest.httpPostWithJSON("https://api.weixin.qq.com/cgi-bin/comment/list?access_token="+accessToken, param);
		System.out.println("查看指定文章的评论数据（新增接口）");
		System.out.println(result);
		JSONObject json = JSONObject.parseObject(result);
		
		if(json.getString("errcode").equals("0")) {
			 JSONArray commentlist = json.getJSONArray("comment");
			for (int j = 0; j < commentlist.size(); j++) {
				JSONObject  comment = commentlist.getJSONObject(j);
				System.out.println(comment.getString("content"));
				System.out.println(comment.getString("openid"));
				System.out.println(comment.getString("create_time"));
				WeiComment info = new WeiComment();
				info.setId(comment.getString("user_comment_id"));
				info.setMsgid(msgid);
				info.setContent(comment.getString("content"));
				JSONObject user = userinfo(accessToken, comment.getString("openid"));
				System.out.println();
				if(user.getString("subscribe").equals("1")) {
					info.setNickname(user.getString("nickname"));
					info.setHeadimgurl(user.getString("headimgurl"));
				}else {
					System.out.println("暂无头像");
				}
				info.setOpenid(comment.getString("openid"));
				info.setType(1);
				info.setCreateTime(DateUtils.TimestampToDate(comment.getInteger("create_time")));
				try {
					weiCommentService.saveOrUpdate(info);
				} catch (Exception e) {
					e.printStackTrace();
				}
			
				System.out.println("-----------------------------------------------------------");
			}
		}
	}
	
	public JSONArray getarticletotal(String accessToken, String day) {
		JSONObject param = new JSONObject();
		param.put("begin_date", day);
		param.put("end_date", day);
	
		String result = HttpRequest.httpPostWithJSON("https://api.weixin.qq.com/datacube/getarticletotal?access_token="+accessToken, param);
		System.out.println("图文分析数据接口");
		System.out.println(result);
		JSONObject json = JSONObject.parseObject(result);
		if(json.containsKey("list")) {
			return json.getJSONArray("list");
		}else {
			return null;
		}
	}
	public  JSONObject userinfo(String accessToken,String openid) {

		
		String result = HttpRequest.sendGet("https://api.weixin.qq.com/cgi-bin/user/info", "access_token="+accessToken+"&openid="+openid+"&lang=zh_CN");
		System.out.println("获取用户信息");
		return JSONObject.parseObject(result);
	}
	
	
	public static void main(String[] args) {
		WeixinCommentJob wx = new WeixinCommentJob();
		String accessToken = wx.get_token("11");
		
		int days = 1;
		
		String start = DateUtils.getYesterday("yyyy-MM-dd", -1*days);
		String end = DateUtils.getYesterday("yyyy-MM-dd", -1);
		String[] msgidlist = {"2652697097_1"};
		for (String msgid : msgidlist) {
			wx.getcommentlist(accessToken, msgid);
		}
	}
	
}

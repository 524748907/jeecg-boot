package org.jeecg.modules.wei.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信微博 群发文章列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
public interface WeiArticleMapper extends BaseMapper<WeiArticle> {
	
	public List queryList(@Param("type") Integer type,@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
	
	public List queryMsgidList(@Param("type") Integer type,@Param("start") String start,@Param("end") String end);
	
	
	public List statClickCountList(@Param("type") Integer type,@Param("start") String start,@Param("end") String end);
	
	public List queryTopList(@Param("type") Integer type,@Param("start") String start,@Param("end") String end,@Param("pageSize") Integer pageSize);
	public List statUserList(@Param("type") Integer type,@Param("start") String start,@Param("end") String end);
	
	public Long queryCountByUid(@Param("uid") String uid);
	
	public List statPinlvList(@Param("today") String today);
	
}

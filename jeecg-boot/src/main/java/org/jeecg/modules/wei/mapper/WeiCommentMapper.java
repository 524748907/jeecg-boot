package org.jeecg.modules.wei.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信微博 评论列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
public interface WeiCommentMapper extends BaseMapper<WeiComment> {

	public Integer queryListCount(@Param("msgid") String msgid);
	public List queryList(@Param("msgid") String msgid,@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
}

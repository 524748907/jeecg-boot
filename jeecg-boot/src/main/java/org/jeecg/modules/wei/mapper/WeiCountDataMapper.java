package org.jeecg.modules.wei.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiCountData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信、微博关注粉丝
 * @author： jeecg-boot
 * @date：   2019-07-05
 * @version： V1.0
 */
public interface WeiCountDataMapper extends BaseMapper<WeiCountData> {
	
	public List statCountList(@Param("type") Integer type,@Param("start") String start,@Param("end") String end);
}

package org.jeecg.modules.wei.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiArticle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信微博 群发文章列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
public interface IWeiArticleService extends IService<WeiArticle> {
	
	public List queryList(Integer type,Integer pageNo, Integer pageSize);
	public List queryMsgidList(Integer type, String start, String end);
	public List statClickCountList(Integer type, String start, String end);
	public List statUserList(Integer type,String start, String end);
	public List queryTopList( Integer type,String start,String end,Integer pageSize);
	public Long queryCountByUid(String uid);
	
	public List statPinlvList(String today);
}

package org.jeecg.modules.wei.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信微博 评论列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
public interface IWeiCommentService extends IService<WeiComment> {
	
	public Integer queryListCount(String msgid);
	
	public List queryList(String msgid, Integer pageNo, Integer pageSize);
}

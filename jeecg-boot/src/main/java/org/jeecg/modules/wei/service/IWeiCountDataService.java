package org.jeecg.modules.wei.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wei.entity.WeiCountData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信、微博关注粉丝
 * @author： jeecg-boot
 * @date：   2019-07-05
 * @version： V1.0
 */
public interface IWeiCountDataService extends IService<WeiCountData> {
	
	public List statCountList( Integer type,String start,String end);
}

package org.jeecg.modules.wei.service.impl;

import java.util.List;

import org.jeecg.modules.wei.entity.WeiArticle;
import org.jeecg.modules.wei.mapper.WeiArticleMapper;
import org.jeecg.modules.wei.service.IWeiArticleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信微博 群发文章列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
@Service
public class WeiArticleServiceImpl extends ServiceImpl<WeiArticleMapper, WeiArticle> implements IWeiArticleService {

	@Override
	public List queryList(Integer type, Integer pageNo, Integer pageSize) {
		return baseMapper.queryList(type, (pageNo-1)*pageSize, pageSize);
	}

	@Override
	public List queryMsgidList(Integer type, String start, String end) {
		return baseMapper.queryMsgidList(type, start, end);
	}

	@Override
	public Long queryCountByUid(String uid) {
		return baseMapper.queryCountByUid(uid);
	}

	@Override
	public List queryTopList(Integer type, String start, String end, Integer pageSize) {
		return baseMapper.queryTopList(type, start, end, pageSize);
	}

	@Override
	public List statClickCountList(Integer type, String start, String end) {
		return baseMapper.statClickCountList(type, start, end);
	}

	@Override
	public List statUserList(Integer type,String start, String end) {
		return baseMapper.statUserList(type, start, end);
	}

	@Override
	public List statPinlvList(String today) {
		return baseMapper.statPinlvList(today);
	}



}

package org.jeecg.modules.wei.service.impl;

import java.util.List;

import org.jeecg.modules.wei.entity.WeiComment;
import org.jeecg.modules.wei.mapper.WeiCommentMapper;
import org.jeecg.modules.wei.service.IWeiCommentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信微博 评论列表
 * @author： jeecg-boot
 * @date：   2019-06-19
 * @version： V1.0
 */
@Service
public class WeiCommentServiceImpl extends ServiceImpl<WeiCommentMapper, WeiComment> implements IWeiCommentService {

	@Override
	public List queryList(String msgid, Integer pageNo, Integer pageSize) {
		return baseMapper.queryList(msgid, (pageNo - 1) * pageSize, pageSize);
	}

	@Override
	public Integer queryListCount(String msgid) {
		return baseMapper.queryListCount(msgid);
	}

}

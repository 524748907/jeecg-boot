package org.jeecg.modules.wei.service.impl;

import java.util.List;

import org.jeecg.modules.wei.entity.WeiCountData;
import org.jeecg.modules.wei.mapper.WeiCountDataMapper;
import org.jeecg.modules.wei.service.IWeiCountDataService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信、微博关注粉丝
 * @author： jeecg-boot
 * @date：   2019-07-05
 * @version： V1.0
 */
@Service
public class WeiCountDataServiceImpl extends ServiceImpl<WeiCountDataMapper, WeiCountData> implements IWeiCountDataService {

	@Override
	public List statCountList(Integer type, String start, String end) {
		return baseMapper.statCountList(type, start, end);
	}

}

/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.jeecg.modules.wei.util;

import java.util.Date;
import java.util.List;

import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.webcrawler.entity.WebCrawlerWord;
import org.jeecg.modules.webcrawler.service.IWebCrawlerArticleService;
import org.jeecg.modules.webcrawler.service.IWebCrawlerWordService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

/**
 * 接口配置工具类
 * 
 * @author Administrator
 *
 */
@Service
public class WeiCacheUtils implements ApplicationContextAware {
	
	private static RedisUtil redisUtil;
	
	

    
    /**
     * 获取文章列表
     * @param uid
     * @return
     */
    public static JSONObject get_article_list(String uid,String pageToken) {
		String key = "toutiao_article_list_"+uid + "_"+pageToken;
		Object result = redisUtil.get(key);
		if (result != null) {
			long time = WeiCacheUtils.getRedisCreateTime(key);
			long nowtime = new Date().getTime();
			System.out.println(time);
			System.out.println((time + 1000 * 60 * 60 * 4));
			System.out.println(nowtime);
			System.out.println(nowtime > (time + 1000 * 60 * 60 * 4));
			if(nowtime > (time + 1000 * 60 * 60 * 4)) {
				System.out.println("已超时");
				JSONObject json = ToutiaoUtils.get_article_list(uid, pageToken);
				if(json != null) {
					redisUtil.set(key, json);
					return json;
				}else {
					System.out.println("未超时");
					return (JSONObject)result;
				}
			}else {
				return (JSONObject)result;
			}
		}else{
			JSONObject json = ToutiaoUtils.get_article_list(uid, pageToken);
			if(json != null) {
				redisUtil.set(key, json);
				return json;
			}else {
				return new JSONObject();
			}
			
		}
	}
    
    public static JSONObject get_article_comment_list(String id,String pageToken) {
		String key = "toutiao_comment_list_"+id + "_" + pageToken;
		Object result = redisUtil.get(key);
		if (result != null) {
			long time = WeiCacheUtils.getRedisCreateTime(key);
			long nowtime = new Date().getTime();
			if(nowtime > (time + 1000 * 60 * 60 * 4)) {
				JSONObject json = ToutiaoUtils.get_article_comment_list(id, pageToken);
				if(json != null) {
					redisUtil.set(key, json);
				}
				return json;
			}else {
				return (JSONObject)result;
			}
		}else{
			JSONObject json = ToutiaoUtils.get_article_comment_list(id, pageToken);
			if(json != null) {
				redisUtil.set(key, json);
			}
			return json;
		}
	}
    
    public static String redis_test(String id,String pageToken) {
		String key = "toutiao_comment_list_"+id + "_" + pageToken;
		Object result = redisUtil.get(key);
		if (result != null) {
			
			return (String)result;
		}else{
			redisUtil.set(key, pageToken);
			return pageToken;
		}
	}
    
    public static long getRedisCreateTime(String key) {
    	key = "time" + key;
    	Object result = redisUtil.get(key);
		if (result != null) {
			return (long)result;
		}else{
			long time = new Date().getTime();
			redisUtil.set(key, time);
			return time;
		}
    }
    
    /**
     * 删除缓存
     * @param key
     */
    public static void del(String key) {
		redisUtil.del(key);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		redisUtil = (RedisUtil) applicationContext.getBean("redisUtil");	
	}
}
